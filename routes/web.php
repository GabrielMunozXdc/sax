<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReportSendMessage;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\SendMessage;
use App\Http\Controllers\ScheduledTaskAlerts;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
})->middleware('auth');

Route::get('/dashboard', function () {
    return view('dashboard', ["messages" => []]); // Puedes pasar cualquier dato en el arreglo asociativo
})->name('dashboard')->middleware('auth');

Route::get('/tables', function () {
    return view('tables');
})->name('tables')->middleware('auth');

Route::get('/wallet', function () {
    return view('wallet');
})->name('wallet')->middleware('auth');

Route::get('/RTL', function () {
    return view('RTL');
})->name('RTL')->middleware('auth');

Route::get('/profile', function () {
    return view('account-pages.profile');
})->name('profile')->middleware('auth');

Route::get('/signin', function () {
    return view('account-pages.signin');
})->name('signin');

Route::get('/signup', function () {
    return view('account-pages.signup');
})->name('signup')->middleware('guest');

Route::get('/sign-up', [RegisterController::class, 'create'])
    ->middleware('guest')
    ->name('sign-up');

Route::post('/sign-up', [RegisterController::class, 'store'])
    ->middleware('guest');

Route::get('/sign-in', [LoginController::class, 'create'])
    ->middleware('guest')
    ->name('sign-in');

Route::post('/sign-in', [LoginController::class, 'store'])
    ->middleware('guest');

Route::post('/logout', [LoginController::class, 'destroy'])
    ->middleware('auth')
    ->name('logout');

Route::get('/forgot-password', [ForgotPasswordController::class, 'create'])
    ->middleware('guest')
    ->name('password.request');

Route::post('/forgot-password', [ForgotPasswordController::class, 'store'])
    ->middleware('guest')
    ->name('password.email');

Route::get('/reset-password/{token}', [ResetPasswordController::class, 'create'])
    ->middleware('guest')
    ->name('password.reset');

Route::post('/reset-password', [ResetPasswordController::class, 'store'])
    ->middleware('guest');

Route::get('/laravel-examples/user-profile', [ProfileController::class, 'index'])->name('users.profile')->middleware('auth');
Route::put('/laravel-examples/user-profile/update', [ProfileController::class, 'update'])->name('users.update')->middleware('auth');
Route::get('/laravel-examples/users-management', [UserController::class, 'index'])->name('users-management')->middleware('auth');


Route::get('/send-message', [SendMessage::class, 'sendAlert']);


use App\Http\Controllers\ConfiguracionController;

// Ruta para mostrar la lista de configuraciones
Route::get('/configuraciones', [ConfiguracionController::class, 'index'])->name('configuraciones.index')->middleware('auth');;

// Ruta para mostrar el formulario de creación
Route::get('/configuraciones/crear', [ConfiguracionController::class, 'create'])->name('configuraciones.create')->middleware('auth');;

// Ruta para procesar el formulario de creación (almacenar)
Route::post('/configuraciones', [ConfiguracionController::class, 'store'])->name('configuraciones.store')->middleware('auth');;

// Ruta para mostrar el detalle de una configuración
Route::get('/configuraciones/{configuracion}', [ConfiguracionController::class, 'show'])->name('configuraciones.show')->middleware('auth');;

// Ruta para mostrar el formulario de edición
Route::get('/configuraciones/{configuracion}/editar', [ConfiguracionController::class, 'edit'])->name('configuraciones.edit')->middleware('auth');;

// Ruta para procesar el formulario de edición
Route::put('/configuraciones/{configuracion}', [ConfiguracionController::class, 'update'])->name('configuraciones.update')->middleware('auth');;

// Ruta para eliminar una configuración
Route::delete('/configuraciones/{configuracion}', [ConfiguracionController::class, 'destroy'])->name('configuraciones.destroy')->middleware('auth');;


Route::get('/run-alert',[ScheduledTaskAlerts::Class,'executeAlert'])->middleware('auth');;
Route::get('/dashboard-data',[ScheduledTaskAlerts::Class,'obtenerAlertasEnJSON'])->middleware('auth');;

Route::get('/alertas-enviadas', [ReportSendMessage::class, 'alertsSent'])->name('reporte.alerts-sent')->middleware('auth');;

Route::post('/alertas-enviadas/filter', [ReportSendMessage::class, 'filter'])->name('reporte.alerts-sent-filter')->middleware('auth');;

Route::get('/exportar/alertas-enviadas', [ReportSendMessage::class, 'exportAlertas'])
    ->name('reporte.alerts-sent.export')->middleware('auth');;

