<x-app-layout>
    <style>
        .disabled-option {
            color: gray; /* Cambia el color de fuente a gris */
            background-color: #f5f5f5; /* Cambia el color de fondo */
            /* Otros estilos según tus preferencias */
        }
    </style>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <x-app.navbar />
        <div class="container-fluid py-4 px-5">
            <form method="POST" action="{{ route('configuraciones.update', $configuracion->id) }}">
                @csrf
                @method('PUT')
                <!-- Agrega los campos faltantes en el formulario de edición -->

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="porcentaje1">Porcentaje 1</label>
                            <select id="porcentaje1" name="porcentaje1" class="form-control" required>
                                @for ($i = 5; $i <= 100; $i += 5)
                                    <option value="{{ $i }}" {{ old('porcentaje1', $configuracion->porcentaje1) == $i ? 'selected' : '' }}>
                                        {{ $i }}%
                                    </option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="porcentaje2">Porcentaje 2</label>
                            <select id="porcentaje2" name="porcentaje2" class="form-control" required>
                                @for ($i = 5; $i <= 100; $i += 5)
                                    <option value="{{ $i }}" {{ old('porcentaje2', $configuracion->porcentaje2) == $i ? 'selected' : '' }}>
                                        {{ $i }}%
                                    </option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="porcentaje3">Porcentaje 3</label>
                            <select id="porcentaje3" name="porcentaje3" class="form-control" required>
                                @for ($i = 5; $i <= 100; $i += 5)
                                    <option value="{{ $i }}" {{ old('porcentaje3', $configuracion->porcentaje3) == $i ? 'selected' : '' }}>
                                        {{ $i }}%
                                    </option>
                                @endfor
                            </select>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label for="agente">Agente</label>
                            <select name="agente" class="form-control" required>
                                <option value="Av" {{ old('agente', $configuracion->agente) === 'Av' ? 'selected' : '' }}>Av</option>
                                <option value="Gold" {{ old('agente', $configuracion->agente) === 'Gold' ? 'selected' : '' }}>Gold</option>
                                <option value="Elite" {{ old('agente', $configuracion->agente) === 'Elite' ? 'selected' : '' }}>Elite</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="cant_alert">Cantidad de Alertas</label>
                            <input type="text" name="cant_alert_" class="form-control" value="1" required disabled>
                            <input type="hidden" name="cant_alert" class="form-control" value="1" required >
                        </div>
                    </div>

                    <div class="col-12">
                        <label for="mensaje_alerta">Mensaje Alerta</label>
                        <textarea name="mensaje_alerta_" id="mensaje_alerta_" cols="30" rows="10" class="form-control" required disabled>Alerta! El tiempo de gestión de la incidencia incidencia del cliente cliente del segmento categoria_cliente se encuentra en un porcentaje de un tiempo de compromiso de compromiso</textarea>
                        <input type="hidden" name="mensaje_alerta" class="form-control" value="Alerta! El tiempo de gestión de la incidencia incidencia del cliente cliente del segmento categoria_cliente se encuentra en un porcentaje de un tiempo de compromiso de compromiso" required >

                    </div>


                    <div class="col-6">
                        <div class="form-group">
                            <label for="numero">Número</label>
                            <input type="text" name="numero" class="form-control" value="{{ old('numero', $configuracion->numero) }}" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control" required>
                                <option value="Activo" {{ old('status', $configuracion->status) === 'Activo' ? 'selected' : '' }}>Activo</option>
                                <option value="Inactivo" {{ old('status', $configuracion->status) === 'Inactivo' ? 'selected' : '' }}>Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                </div>
            
                <!-- Resto del formulario y botón de envío -->
            </form>
        </div>
        
    </main>

    <script>
       const porcentaje1Select = document.getElementById('porcentaje1');
    const porcentaje2Select = document.getElementById('porcentaje2');
    const porcentaje3Select = document.getElementById('porcentaje3');

    function updateOptions() {
        // Valor seleccionado en el primer select
        const porcentaje1Value = parseInt(porcentaje1Select.value);
        // Valor seleccionado en el segundo select
        const porcentaje2Value = parseInt(porcentaje2Select.value);

        // Deshabilitar opciones en el segundo select según la selección del primer select
        for (const option of porcentaje2Select.options) {
        option.disabled = parseInt(option.value) <= porcentaje1Value;
        if (option.disabled) {
            option.classList.add('disabled-option'); // Agrega la clase 'disabled-option'
        } else {
            option.classList.remove('disabled-option'); // Elimina la clase 'disabled-option' si estaba habilitada anteriormente
        }
    }

    // Deshabilitar opciones en el tercer select según la selección del segundo select
    for (const option of porcentaje3Select.options) {
        option.disabled = parseInt(option.value) <= porcentaje2Value;
        if (option.disabled) {
            option.classList.add('disabled-option'); // Agrega la clase 'disabled-option'
        } else {
            option.classList.remove('disabled-option'); // Elimina la clase 'disabled-option' si estaba habilitada anteriormente
        }
    }
    }

    // Manejar cambios en los selects
    porcentaje1Select.addEventListener('change', updateOptions);
    porcentaje2Select.addEventListener('change', updateOptions);

    // Ejecutar la función al cargar la página
    updateOptions();
</script>

    
    
</x-app-layout>


