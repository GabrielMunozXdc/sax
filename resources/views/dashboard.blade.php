<x-app-layout>

    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <x-app.navbar />
        <div class="container-fluid py-4 px-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-md-flex align-items-center mb-3 mx-2">
                        <div class="mb-md-0 mb-3">
                            <h3 class="font-weight-bold mb-0">Hola, Supervisor</h3>
                            <p class="mb-0">¡Aplicaciones que te pueden gustar!</p>
                        </div>
                        <button type="button"
                            class="btn btn-sm btn-white btn-icon d-flex align-items-center mb-0 ms-md-auto mb-sm-0 mb-2 me-2">
                            <span class="btn-inner--icon">
                                <span class="p-1 bg-success rounded-circle d-flex ms-auto me-2">
                                    <span class="visually-hidden">New</span>
                                </span>
                            </span>
                            <span class="btn-inner--text">Messages</span>
                        </button>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-sm-6 mb-xl-0">
                    <div class="card border shadow-xs mb-4">
                        <div class="card-body text-start p-3 w-100">
                            <div>
                                <i class="fa-solid fa-message"
                                    style="font-size: 20px;color: #ffffff;background: #fcab40;padding: 8px;border-radius: 4px;"></i>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="w-100">
                                        <p class="text-sm text-secondary mb-1 mt-2">Alertas enviadas</p>
                                        <h4 class="mb-2 font-weight-bold" id="alt_env"># 0</h4>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-6 mb-xl-0">
                    <div class="card border shadow-xs mb-4">
                        <div class="card-body text-start p-3 w-100">
                            <div>
                                <i class="fa-solid fa-circle-exclamation"
                                    style="font-size: 20px;color: #ffffff;background: #fcab40;padding: 8px;border-radius: 4px;"></i>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="w-100">
                                        <p class="text-sm text-secondary mb-1 mt-2">Total de alertas</p>
                                        <h4 class="mb-2 font-weight-bold" id="alt_tol"># 0</h4>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-6 mb-xl-0">
                    <div class="card border shadow-xs mb-4">
                        <div class="card-body text-start p-3 w-100">
                            <div>
                                <i class="fa-solid fa-message"
                                    style="font-size: 20px;color: #ffffff;background: #fcab40;padding: 8px;border-radius: 4px;"></i>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="w-100">
                                        <p class="text-sm text-secondary mb-1 mt-2">Envios fallídos</p>
                                        <h4 class="mb-2 font-weight-bold" id="alt-fal"># 0</h4>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <hr class="my-0">
            <div class="row my-4">
                <div class="col-lg-12 col-md-12">
                    <div class="card shadow-xs border">
                        <div class="card-header border-bottom pb-0">
                            <div class="d-sm-flex align-items-center mb-3">
                                <div>
                                    <h6 class="font-weight-semibold text-lg mb-0">Lista de alertas</h6>
                                    <p class="text-sm mb-sm-0 mb-2">alertas ejecutadas mediante el CronJob</p>
                                </div>
                                <div class="ms-auto d-flex">
                                    <button type="button" class="btn btn-sm btn-white mb-0 me-2">
                                        View report
                                    </button>
                                    <a href="{{ route('reporte.alerts-sent.export') }}" style="color:white">

                                        <button type="button"
                                            class="btn btn-sm btn-dark btn-icon d-flex align-items-center mb-0">

                                            <span class="btn-inner--icon">
                                                <svg width="16" height="16" xmlns="http://www.w3.org/2000/svg"
                                                    fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                    stroke="currentColor" class="d-block me-2">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5M16.5 12L12 16.5m0 0L7.5 12m4.5 4.5V3" />
                                                </svg>
                                            </span>
                                            <span class="btn-inner--text">Download</span>
                                        </button>
                                    </a>

                                </div>
                            </div>

                        </div>
                        <div class="card-body px-0 py-0">
                            <div class="table-responsive p-0">
                                <table class="table align-items-center justify-content-center mb-0">
                                    <thead class="bg-gray-100">
                                        <tr>
                                            <th class="text-secondary text-xs font-weight-semibold opacity-7">
                                                id</th>
                                            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                                                Incidente</th>
                                            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                                                Segmento</th>
                                            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                                                Margen ejecuta
                                            </th>
                                            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                                                Fecha</th>
                                            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                                                Estado</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($messages as $message)
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2">
                                                    <div class="my-auto">
                                                        <h6 class="mb-0 text-sm">Mess {{ $message->id }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="my-auto text-sm font-weight-normal ">{{ $message->incident }}
                                                </p>
                                            </td>
                                            <td>
                                                <p class="my-auto text-sm font-weight-normal ">{{ $message->segment_sent
                                                    }} </p>
                                            </td>
                                            <td>
                                                <p class="my-auto text-sm font-weight-normal">{{
                                                    $message->threshold_executed }} %</p>
                                            </td>
                                            <td class="align-middle">
                                                <p class="my-auto text-sm font-weight-normal">{{ $message->date_time }}
                                                </p>
                                            </td>
                                            <td>
                                                <p class="text-sm font-weight-normal mb-0"> {{ $message->status }}</p>
                                            </td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script>
        $(document).ready(function () {
                $.ajax({
                    url: '/dashboard-data', // Reemplaza por la URL correcta de tu controlador
                    method: 'GET', // Puedes usar 'POST' si lo prefieres
                    dataType: 'json', // Especifica que esperas una respuesta en formato JSON
                    success: function (data) {
                        $("#alt_env").html("#" + data.alertasEnviadas );
                        $("#alt_tol").html("#" + data.alertasTotales);
                        $("#alt-fal").html("#" + data.alertasFallidas);
                    },
                    error: function (xhr, status, error) {
                        console.log("Error en la solicitud AJAX: " + error);
                    }
                });
        });
    </script>


</x-app-layout>