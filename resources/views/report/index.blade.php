<x-app-layout>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <x-app.navbar />
    
        <div class="container-fluid py-4 px-5">
            <form action="{{ route('reporte.alerts-sent-filter') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-6">
                        <label for="">Fecha de Inicio</label>
                        <input type="datetime-local" class="form-control" name="fecha_inicio" placeholder="Fecha de inicio">
                    </div>
                    <div class="col-6">
                        <label for="">Fecha de Fin</label>
                        <input type="datetime-local" class="form-control" name="fecha_fin" placeholder="Fecha de fin">
                    </div>
                    <div class="col-6 mt-3">
                        <label for="">Segmento</label>

                        <select class="form-control" name="segmento[]" multiple>
                            <option value="Av">Av</option>
                            <option value="Gold">Gold</option>
                            <option value="Elite">Elite</option>
                        </select>
                    </div>
                    <div class="col-6 mt-3">
                        <label for="">Estado</label>
                        <select class="form-control" name="estado[]" multiple>
                            <option value="Enviado">Enviado</option>
                            <option value="No enviado">No enviado</option>
                        </select>
                    </div>
                    <div class="text-center mt-4">
                        <button class="btn btn-primary" type="submit">Filtrar</button>

                    </div>
                </div>
            </form>
            <div class="row my-4">
                <div class="col-lg-12 col-md-12">
                    <div class="card shadow-xs border">
                        <div class="card-header border-bottom pb-0">
                            <div class="d-sm-flex align-items-center mb-3">
                                <div>
                                    <h6 class="font-weight-semibold text-lg mb-0">Lista de alertas</h6>
                                    <p class="text-sm mb-sm-0 mb-2">alertas ejecutadas mediante el CronJob</p>
                                </div>
                                <div class="ms-auto d-flex">
                                    <button type="button" class="btn btn-sm btn-white mb-0 me-2">
                                        View report
                                    </button>
                                    <a href="{{ route('reporte.alerts-sent.export') }}" style="color:white">

                                    <button type="button"
                                        class="btn btn-sm btn-dark btn-icon d-flex align-items-center mb-0">

                                        <span class="btn-inner--icon">
                                            <svg width="16" height="16" xmlns="http://www.w3.org/2000/svg"
                                                fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                stroke="currentColor" class="d-block me-2">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                    d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5M16.5 12L12 16.5m0 0L7.5 12m4.5 4.5V3" />
                                            </svg>
                                        </span>
                                        <span class="btn-inner--text">Download</span>
                                    </button>
                                </a>

                                </div>
                            </div>
                            
                        </div>
                        <div class="card-body px-0 py-0">
                            <div class="table-responsive p-0">
                                <table class="table align-items-center justify-content-center mb-0">
                                    <thead class="bg-gray-100">
                                        <tr>
                                            <th class="text-secondary text-xs font-weight-semibold opacity-7">
                                                id</th>
                                            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                                                Incidente</th>
                                                <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                                                    Segmento</th>
                                            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">Margen ejecuta
                                            </th>
                                            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                                                Fecha</th>
                                            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                                                    Estado</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($messages as $message)
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2">
                                                    <div class="my-auto">
                                                        <h6 class="mb-0 text-sm">Mess {{ $message->id }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="my-auto text-sm font-weight-normal ">{{ $message->incident }} </p>
                                            </td>  
                                            <td>
                                                <p class="my-auto text-sm font-weight-normal ">{{ $message->segment_sent }} </p>
                                            </td>
                                            <td>
                                                <p class="my-auto text-sm font-weight-normal">{{ $message->threshold_executed }} %</p>
                                            </td>
                                            <td class="align-middle">
                                                <p class="my-auto text-sm font-weight-normal">{{ $message->date_time }} </p>
                                            </td>
                                            <td >
                                                <p class="text-sm font-weight-normal mb-0"> {{ $message->status }}</p>
                                            </td>
                                           
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</x-app-layout>
