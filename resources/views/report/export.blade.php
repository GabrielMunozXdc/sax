<table class="table align-items-center justify-content-center mb-0">
    <thead class="bg-gray-100">
        <tr>
            <th class="text-secondary text-xs font-weight-semibold opacity-7">
                id</th>
            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                Incidente</th>
                <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                    Segmento</th>
            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">Margen ejecuta
            </th>
            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                Fecha</th>
            <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">
                    Estado</th>
           
        </tr>
    </thead>
    <tbody>
        @foreach($messages as $message)
        <tr>
            <td>
                <div class="d-flex px-2">
                    <div class="my-auto">
                        <h6 class="mb-0 text-sm">Mess {{ $message->id }}</h6>
                    </div>
                </div>
            </td>
            <td>
                <p class="my-auto text-sm font-weight-normal ">{{ $message->incident }} </p>
            </td>  
            <td>
                <p class="my-auto text-sm font-weight-normal ">{{ $message->segment_sent }} </p>
            </td>
            <td>
                <p class="my-auto text-sm font-weight-normal">{{ $message->threshold_executed }} %</p>
            </td>
            <td class="align-middle">
                <p class="my-auto text-sm font-weight-normal">{{ $message->date_time }} </p>
            </td>
            <td >
                <p class="text-sm font-weight-normal mb-0"> {{ $message->status }}</p>
            </td>
           
        </tr>
    @endforeach
    </tbody>
</table>