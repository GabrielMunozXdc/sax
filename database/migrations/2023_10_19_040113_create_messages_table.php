<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->text('message');
            $table->integer('threshold_executed');
            $table->string('segment_sent');
            $table->dateTime('date_time');
            $table->string('status'); // Puedes usar VARCHAR u otro tipo según tus necesidades
            $table->string('incident'); // Puedes usar VARCHAR u otro tipo según tus necesidades
            $table->integer('umbral'); // Puedes usar VARCHAR u otro tipo según tus necesidades
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
