<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigurationsTable extends Migration
{
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('porcentaje1');
            $table->unsignedInteger('porcentaje2');
            $table->unsignedInteger('porcentaje3');
            $table->string('agente');
            $table->integer('cant_alert');
            $table->text('mensaje_alerta');
            $table->string('numero');
            $table->string('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('configurations');
    }
}

