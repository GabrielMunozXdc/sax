<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParadasTable extends Migration
{
    public function up()
    {
        Schema::create('paradas', function (Blueprint $table) {
            $table->id();
            $table->string('incidencia');
            $table->datetime('fecha_hora_inicio')->nullable();
            $table->datetime('fecha_hora_fin')->nullable();
            $table->integer('diferencia_minutos')->nullable();
            $table->string('estado');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('paradas');
    }
}
