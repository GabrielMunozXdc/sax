<?php

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Message; // Asegúrate de importar el modelo correspondiente


class AlertasExport implements FromCollection
{
    public function collection()
    {
        // Aquí debes obtener tus datos de alerta y devolverlos
        return message::all();
    }
}
