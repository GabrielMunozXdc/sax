<?php

namespace App\Services;

use mysqli;

class MysqliDatabaseService
{
    protected $conn;

    public function __construct()
    {
        // Configura los parámetros de conexión
        $host = '10.161.174.64';
        $port = 3306;
        $username = 'xorex';
        $password = 'X0r3xT3l3f0n1c4++';
        $database = 'ggsre';

        // Crea una instancia de MySQLi
        $this->conn = new mysqli($host, $username, $password, $database, $port);

        // Verifica la conexión
        if ($this->conn->connect_error) {
            throw new \Exception("Error en la conexión a la base de datos: " . $this->conn->connect_error);
        }
    }

    public function consultaVistaAlarma()
    {
        // Consulta SQL
        $sql = "SELECT * FROM vw_alarma_ws_xorex";

        // Ejecuta la consulta
        $result = $this->conn->query($sql);

        // Procesa los resultados según sea necesario
        if ($result) {
            $results = [];
            while ($row = $result->fetch_assoc()) {
                $results[] = $row;
            }
            return $results;
        } else {
            throw new \Exception("Error en la consulta SQL: " . $this->conn->error);
        }
    }

    public function cerrarConexion()
    {
        $this->conn->close();
    }
}
