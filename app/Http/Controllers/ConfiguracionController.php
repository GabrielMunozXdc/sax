<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Configuration; // Asegúrate de importar el modelo correspondiente

class ConfiguracionController extends Controller
{
    public function index()
    {
        $configuraciones = Configuration::all();
        return view('configuration.index', compact('configuraciones'));
    }

    public function create()
    {
        return view('configuration.create');
    }

    public function store(Request $request)
    {

        $request->validate([
            'porcentaje1' => 'required|integer',
            'porcentaje2' => 'required|integer|gt:porcentaje1',
            'porcentaje3' => 'required|integer|gt:porcentaje2',
            'agente' => 'required',
            'cant_alert' => 'required|integer',
            'mensaje_alerta' => 'required',
            'numero' => 'required',
            'status' => 'required',
        ]);

        Configuration::create($request->all());


        return redirect()->route('configuraciones.index')
            ->with('success', 'Configuración guardada con éxito');
    }

    public function show($id)
    {
        $configuracion = Configuration::find($id);
        return view('configuration.show', compact('configuracion'));
    }

    public function edit($id)
    {
        $configuracion = Configuration::find($id);
        return view('configuration.edit', compact('configuracion'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'porcentaje1' => 'required|integer',
            'porcentaje2' => 'required|integer|gt:porcentaje1',
            'porcentaje3' => 'required|integer|gt:porcentaje2',
            'agente' => 'required',
            'cant_alert' => 'required|integer',
            'mensaje_alerta' => 'required',
            'numero' => 'required',
            'status' => 'required',
        ]);

        $configuracion = Configuration::find($id);
        $configuracion->update($request->all());

        return redirect()->route('configuraciones.index')
            ->with('success', 'Configuración actualizada con éxito');
    }

    public function destroy($id)
    {
        $configuracion = Configuration::find($id);
        $configuracion->delete();

        return redirect()->route('configuration.index')
            ->with('success', 'Configuración eliminada con éxito');
    }
}
