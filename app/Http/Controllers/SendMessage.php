<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SendMessage extends Controller
{

    public function sendAlert($dataVar,$phoneNumbers)
    {
        $url = 'https://graph.facebook.com/v17.0/146222008575375/messages';
        $headers = [
            'Authorization: Bearer EAAMVlRJHrygBOzabQfCjEDbc2nLv3VcBWY8W77hYGRG6SNAmiPjCAr6hbRva2fncROXhpweniwjNKB1SzFZAgYG8oXEJWSDCrdLr9Qx3mFiR3RWQSuVgBUB5ZAO0r4EC9X0chhH5y2ZCQZB5smLp2gmgbVV3I69y3n1vmuwWJRsTJAzTqAUPkzViVRhOKzyLp8FV8TQ26ZCZCqYcY6CulYCzbu8EZBNYdUntwZDZD',
            'Content-Type: application/json',
        ];

        // Divide el string de números por coma y conviértelo en un array
        $phoneNumbersArray = explode(',', $phoneNumbers);

        // Itera sobre los números y envía el mensaje a cada uno
        foreach ($phoneNumbersArray as $phoneNumber) {
            // Construye el arreglo de datos para el mensaje con las dataVar
            $data = [
                'messaging_product' => 'whatsapp',
                'to' => "whatsapp://chat?code=$phoneNumber",
                'type' => 'template',
                'template' => [
                    'name' => 'alarmas_xorex',
                    'language' => [
                        'code' => 'es',
                    ],
                    'components' => [
                        [
                            'type' => 'body',
                            'parameters' => [
                                [
                                    'type' => 'text',
                                    'text' => "$dataVar[0]%",
                                ],
                                [
                                    'type' => 'text',
                                    'text' => "$dataVar[1]",
                                ],
                                [
                                    'type' => 'text',
                                    'text' => "$dataVar[2]",
                                ],
                                [
                                    'type' => 'text',
                                    'text' => "$dataVar[3]",
                                ],
                                [
                                    'type' => 'text',
                                    'text' => "$dataVar[4]",
                                ],
                                [
                                    'type' => 'text',
                                    'text' => "porcentaje transcurrido $dataVar[5]%",
                                ],
                            ],
                        ],
                    ],
                ],
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

            $response = curl_exec($ch);
            curl_close($ch);

            // Aquí puedes procesar la respuesta o retornarla según tus necesidades
            // Puedes almacenar las respuestas en un array si lo necesitas
            $responses[] = $response;
        }

        // Puedes retornar las respuestas si es necesario
        return $responses;
    }
}
