<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MysqliDatabaseService;
use App\Models\Configuration;
use App\Http\Controllers\SendMessage;
use App\Models\Message;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use DateTime;


class ScheduledTaskAlerts extends Controller
{
    protected $mysqliService;

    public function __construct(MysqliDatabaseService $mysqliService)
    {
        $this->mysqliService = $mysqliService;
    }

    public function executeAlert()
    {
        
        // Usar la conexión y las consultas a través del servicio
        $resultados = $this->mysqliService->consultaVistaAlarma();
        //dd($resultados);
        $resultadosConPorcentajes = [];
        $alertasSave = [];
        $alertasAEnviar = [];
        //Agregar paradas de las alertas en la tabla paradas
        $agregarParadaDesdeIncidencia = $this->agregarParadaDesdeIncidencia($resultados);


        foreach ($resultados as $resultado) {

             // Obtén el agente de tus resultados
            $agente = $resultado['categoria_cliente'];

             // Obtén el incidencia de tus resultados
            $incidente = $resultado['incidencia'];
        
            // Llama a la función para obtener la configuración del agente
            $configuracionAgente = $this->getConfigByAgent($agente);
        
            // Verifica si ya se ha enviado una notificación para esta configuración
            $notificacionEnviada = $this->verificarAlertas($agente,$incidente);

            // Calcula el porcentaje de avance
            $porcentajeAvance = $this->calculationPorcentajeAvance($resultado['fecha_creacion'], $resultado['compromiso'],$resultado["tiempo_pendiente"]);

        
            if (intval($porcentajeAvance) != 100) {

                $fechaHoraActual = Carbon::now(); 
                $formato = 'Y-m-d h:i:s';
                $fechaHoraFormateada = $fechaHoraActual->format($formato);

                // Define los tres umbrales en un arreglo
                $umbrales = [
                    ['umbral' => 1, 'porcentaje' => $configuracionAgente["data"]['porcentaje1']],
                    ['umbral' => 2, 'porcentaje' => $configuracionAgente["data"]['porcentaje2']],
                    ['umbral' => 3, 'porcentaje' => $configuracionAgente["data"]['porcentaje3']],
                ];

                
                $umbralEncajaMas = 0; // Inicializa la variable para almacenar el umbral correspondiente

                foreach ($umbrales as $umbral) {
                    if ($porcentajeAvance >= $umbral['porcentaje']) {
                        
                        $umbralEncajaMas = $umbral['porcentaje'];
                    } 
                    
                }

                if($umbralEncajaMas != 0){
                    
                    $fechafinCompromiso = $this->convertirCompromisoAFechaHora($resultado["fecha_inicio_afec"], intval($resultado["compromiso"]),$resultado["tiempo_pendiente"]);
                    $message = "Alerta! Queda en un " . 100 - $porcentajeAvance . "% del tiempo de SLA de la incidencia " . $resultado['incidencia'] . " del cliente " . $resultado["cliente"] . " del segmento " . $resultado["categoria_cliente"] .".". " Vencimiento " . $fechafinCompromiso ." - porcentaje transcurrido ". $porcentajeAvance."%";
    
                    $templaneSend = [
                        100 - $porcentajeAvance,
                        $resultado['incidencia'],
                        $resultado["cliente"],
                        $resultado["categoria_cliente"],
                        $fechafinCompromiso,
                        $porcentajeAvance,
                    ];
    
                    $alerta = [
                        'message' => $message,
                        'threshold_executed' => $porcentajeAvance,
                        'segment_sent' => $resultado['categoria_cliente'],
                        'date_time' => $fechaHoraFormateada,
                        "status"=> "Enviado",
                        "incident"=>$resultado['incidencia'],
                        "umbral"=>$umbralEncajaMas
                    ];
    
                    $alertasAEnviar[] = $templaneSend;
                    $alertasSave[] = $alerta;
                }
    
            }
        }

        dd($alertasSave);
        //$this->sendMessageTemplate($alertasAEnviar,$configuracionAgente["data"]["numero"]);
        //$this->saveMessageSend($alertasSave);

        // Cierra la conexión cuando hayas terminado
        $this->mysqliService->cerrarConexion();

        return $alertasSave;
    }

    public function saveMessageSend($arrayAlertSend) {
        $successMessages = [];
        $duplicateMessages = [];
    
        foreach ($arrayAlertSend as $alertSend) {
            $incident = $alertSend['incident'];
            $umbral = $alertSend['umbral'];
    
            // Verifica si ya existe un registro con el mismo incidente y umbral
            $existingMessage = Message::where('incident', $incident)->where('umbral', $umbral)->first();
    
            if (!$existingMessage) {
                // No se encontró un registro existente, por lo que puedes crear uno nuevo
                $alertModels = Message::create($alertSend);
                $successMessages[] = "Alerta para incidente $incident y umbral $umbral registrada exitosamente.";
            } else {
                // Ya existe un registro con el mismo incidente y umbral
                $duplicateMessages[] = "Alerta para incidente $incident y umbral $umbral ya existe en la base de datos.";
            }
        }
    
        $response = [
            'success' => $successMessages,
            'duplicates' => $duplicateMessages,
        ];
    
        return $response;
    }
    

    public function sendMessageTemplate($arraySendMessage,$phoneNumber){

        foreach ($arraySendMessage as $message){
            $SendMessage = new SendMessage();
            $responses = $SendMessage->sendAlert($message, $phoneNumber);
        }

        return true;
    }

    public function getConfigByAgent($agent)
    {
        try {
            $configuration = Configuration::where('agente', $agent)->first()->toArray();

            if ($configuration) {
                return ['data' => $configuration];
            } else {
                return response()->json(['message' => 'Configuración no encontrada'], 404);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error al consultar la configuración'], 500);
        }
    }

    public function verificarAlertas($agente,$incidente)
    {
        // Obtén la configuración para el agente específico
        $configuracion = Configuration::where('agente', $agente)->first();

        if ($configuracion) {
            // Obtén los umbrales de la configuración
            $umbral1 = $configuracion->porcentaje1;
            $umbral2 = $configuracion->porcentaje2;
            $umbral3 = $configuracion->porcentaje3;

            // Obtén el último mensaje para esa configuración
            $ultimoMensaje = Message::where('incident', $incidente)
                ->orderBy('created_at', 'desc')
                ->first();

            // Verifica si el último mensaje cumple con algún umbral
            if ($ultimoMensaje) {
                $umbralEjecutado = $ultimoMensaje->umbral;
                if ($umbralEjecutado >= $umbral1) {
                    $ultimoMensaje->umbral_activado = $umbral1;
                } elseif ($umbralEjecutado >= $umbral2) {
                    $ultimoMensaje->umbral_activado = $umbral2;
                } elseif ($umbralEjecutado >= $umbral3) {
                    $ultimoMensaje->umbral_activado = $umbral3;
                }
            }

            // Realiza acciones basadas en la existencia de alertas
            if (isset($ultimoMensaje->umbral_activado)) {
                return ["status"=>true,"umbral"=>$ultimoMensaje->umbral_activado,"agent"=>$agente];
            } else {
                return ["status"=>false,"umbral"=>0,"agent"=>$agente];
            }
        } else {
            return "No se encontró una configuración para el agente $agente.";
        }
    }

    public function calculationPorcentajeAvance($fechaCreacion, $compromiso, $tiempoEnEspera)
    {

        //Cuantos segundos hay entre la fecha de creacion y ahora
        //


        // Obtén la fecha actual
        $fechaActual = new \DateTime();

        // Obtén la fecha de creación de la incidencia
        $fechaCreacion = new \DateTime($fechaCreacion);

        // Añade el tiempo en espera al compromiso
        $nuevaFechaCompromiso = clone $fechaCreacion;
        $nuevaFechaCompromiso->add(new \DateInterval('PT' . $tiempoEnEspera . 'S'));

        // Calcula el tiempo total de trabajo (sin contar el tiempo en espera)
        $intervalTrabajo = $fechaActual->getTimestamp() - $nuevaFechaCompromiso->getTimestamp();

        // Calcula el porcentaje de avance
        $avancePorcentaje = ($intervalTrabajo / $compromiso) * 100;

        // Asegúrate de que el porcentaje esté en el rango [0, 100]
        $avancePorcentaje = max(0, min(100, $avancePorcentaje));

        return number_format($avancePorcentaje, 0); // Retorna el porcentaje de avance sin decimales
    }



    public function agregarParadaDesdeIncidencia($incidenciaData)
    {

        foreach ($incidenciaData as $incidenciaData) {
            $incidencia_id = $incidenciaData['incidencia'];
            $fecha_inicio_pendiente = $incidenciaData['fecha_inicio_pendiente'];
            $fecha_fin_pendiente = $incidenciaData['fecha_fin_pendiente'];
            $estado = $incidenciaData['estatus'];
        
            // Inicializa la diferencia en minutos en 0
            $diferenciaMinutos = 0;
        
            // Si la fecha de fin de la parada no es "0", calcula la diferencia en minutos
            if ($fecha_fin_pendiente != null) {
                $diferenciaMinutos = Carbon::parse($fecha_fin_pendiente)->diffInMinutes(Carbon::parse($fecha_inicio_pendiente));
            }
        
            // Verifica si ya existe un registro con la misma incidencia_id y las mismas fechas
            $registroExistente = DB::table('paradas')
                ->where('incidencia', $incidencia_id)
                ->where('fecha_hora_inicio', $fecha_inicio_pendiente)
                ->where('fecha_hora_fin', $fecha_fin_pendiente)
                ->first();
        
            // Si no existe un registro con las mismas fechas, inserta un nuevo registro
            if (!$registroExistente) {
                DB::table('paradas')->insert([
                    'incidencia' => $incidencia_id,
                    'fecha_hora_inicio' => $fecha_inicio_pendiente,
                    'fecha_hora_fin' => $fecha_fin_pendiente,
                    'diferencia_minutos' => $diferenciaMinutos,
                    'estado' => $estado,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        }
        
    }

   
    function convertirCompromisoAFechaHora($fechaInicio, $duracionSegundos, $tiempoPendienteSegundos) {
        // Convierte la fecha de inicio en un objeto DateTime
        $fechaInicio = new DateTime($fechaInicio);
    
        // Suma los segundos de duración
        $fechaCompromiso = $fechaInicio->modify("+" . $duracionSegundos . " seconds");
    
        // Suma el tiempo pendiente en segundos
        $fechaCompromiso->modify("+" . $tiempoPendienteSegundos . " seconds");
    
        // Formatea la fecha de compromiso
        $fechaCompromisoFormateada = $fechaCompromiso->format("Y-m-d H:i:s");
    
        return $fechaCompromisoFormateada;
    }
    

    function obtenerAlertasEnJSON() {
        $alertasEnviadas = DB::table('messages')->where('status', 'Enviado')->count();
        $alertasFallidas = DB::table('messages')->where('status', 'fallida')->count();
        $alertasVista = $this->mysqliService->consultaVistaAlarma();
    
        return response()->json([
            'alertasEnviadas' => $alertasEnviadas,
            'alertasFallidas' => $alertasFallidas,
            'alertasTotales' => count($alertasVista),
        ]);
    }
    
}
