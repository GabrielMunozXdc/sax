<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;


class ReportSendMessage extends Controller
{
    public function alertsSent (){
        $messages = Message::all();
        return view('report.index', compact('messages'));
    }

    public function filter(Request $request)
    {
        $query = Message::query();

        if ($request->input('fecha_inicio')) {
            $query->where('date_time', '>=', $request->input('fecha_inicio'));
        }

        if ($request->input('fecha_fin')) {
            $query->where('date_time', '<=', $request->input('fecha_fin'));
        }

        if ($request->input('segmento')) {
            $query->whereIn('segment_sent', $request->input('segmento'));
        }

        if ($request->input('estado')) {
            $query->whereIn('status', $request->input('estado'));
        }

        $messages = $query->paginate(10); // Ajusta la cantidad por página según tus necesidades

        return view('report.index', compact('messages'));

    }

    public function exportAlertas()
    {
        $messages = Message::all();
         // Renderiza la vista export.blade.php con los datos
         $view = View::make('report.export', ['messages' => $messages]);

         // Crea una respuesta con tipo de contenido Excel
         $response = new Response($view, 200);
         $response->header('Content-Type', 'application/vnd.ms-excel');
         $response->header('Content-Disposition', 'attachment; filename=exported_data.xls');
 
         return $response;
    }
}
