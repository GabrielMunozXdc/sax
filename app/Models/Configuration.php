<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $fillable = [
        'porcentaje1', 'porcentaje2', 'porcentaje3', 'agente', 'cant_alert', 'mensaje_alerta', 'numero', 'status'
    ];
}
