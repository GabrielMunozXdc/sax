<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'message',
        'threshold_executed',
        'segment_sent',
        'date_time',
        'status',
        "incident",
        "umbral"
    ];

    // Otros métodos y relaciones del modelo si es necesario
}
